package sarp

import (
	"encoding/json"
	"log"
	"time"
)

/*

	a simple store to stack sniffer events by mac address until we receive the final Ack



*/

type snifferEvents struct {
	store map[string]snifferEvent
}

func NewStore() *snifferEvents {
	return &snifferEvents{store: make(map[string]snifferEvent)}
}

func (s *snifferEvents) Set(key string, value snifferEvent) {
	s.store[key] = value
}

func (s *snifferEvents) Get(key string) (value snifferEvent, ok bool) {
	value, ok = s.store[key]
	return value, ok
}

//
//
//

// struct to store sniffer messages
type snifferEvent struct {
	Timestamp time.Time              // the time stamp of start
	Data      []byte                 // the body of sniffer message
	Request   map[string]interface{} // the stored Request
	Ack       map[string]interface{} // the stored reply

}

func NewSnifferEvent(request []byte) (ev *snifferEvent, err error) {
	ev = &snifferEvent{
		Data:      request,
		Request:   make(map[string]interface{}), // the stored Request
		Ack:       make(map[string]interface{}), // the stored Ack
		Timestamp: time.Now(),
	}
	err = ev.AddRequest(request)
	return ev, err
}

func (s *snifferEvent) AddRequest(data []byte) error {
	err := json.Unmarshal(data, &s.Request)
	if err != nil {
		log.Printf("Dora EventMaker: not a valid json sniffer dhcp request\n")
	}
	return err
}

func (s *snifferEvent) AddAck(data []byte) error {
	err := json.Unmarshal(data, &s.Ack)
	if err != nil {
		log.Printf("Dora EventMaker: not a valid json sniffer dhcp Ack\n")
	}
	return err
}

// GetAck return json representation of ACK
func (s *snifferEvent) GetAck() (data []byte, err error) {
	data, err = json.Marshal(s.Ack)
	return data, err
}

//  UpdateAckWithRequest: add some fields of the Request to the Ack
func (s *snifferEvent) UpdateAckWithRequest(field ...string) {
	// for each field update Ack map with request map
	for _, f := range field {
		// see if field present in Ack
		_, ok := s.Ack[f]
		if !ok {
			// not present add it if present in request
			value, ok := s.Request[f]
			if ok {
				s.Ack[f] = value
			}
		}
	}
}
