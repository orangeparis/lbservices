package _nping

import (
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
)

var pingBody = `PING`
var pongBody = `PONG`

var natsTimeoutError = "nats: timeout"

// return subject get.service.ping
func pingSubject(service string) string {
	return fmt.Sprintf("get.%s.ping", service)
}

type Client struct {
	*nats.Conn
}

func (c *Client) Ping(service string, timeout int) (bool, error) {
	return Ping(c.Conn, service, timeout)
}

func (c *Client) HandlePingService(service string) (sub *nats.Subscription, err error) {
	return HandlePingService(c.Conn, service)
}

//// check if serice available
//func (c *Client) IsService(service string) bool {
//
//	ok,err := c.Ping(service,1)
//	if ok == true {
//		// service is in use
//		return true
//	}
//	// an error occured
//	if err != nil {
//		// error occurred either a timeout or coneection error
//	}
//
//}

func NewClient(natsServer string) (client *Client, err error) {

	// open a nats connection
	nc, err := nats.Connect(natsServer)
	if err != nil {
		log.Printf("nping client: cannot open nats connection: %s", err.Error())
		return client, err
	}

	client = &Client{nc}
	return client, nil
}
