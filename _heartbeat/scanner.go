package _heartbeat

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
	"strings"
)

/*
	scanner

	subscribe to  Heartbeat.>

	* update the heartbeat store
	* create metrics for prometheus


*/

// subscribe topic Heartbeat.>
var topic = RootTopic + ".>"

func Scan(ctx context.Context, natsUrl string, store Store) error {

	// create nats connection
	nc, err := nats.Connect(natsUrl)
	if err != nil {
		log.Printf("Heartbeat scanner: cannot open nats connection: %s", err.Error())
		return err
	}
	defer nc.Close()
	msg := fmt.Sprintf("Heartbeat scanner connected with nats at : %s\n", natsUrl)
	log.Println(msg)

	// subscribe to all inputs
	_, err = nc.Subscribe(topic, func(m *nats.Msg) {
		log.Printf("heartbeat scanner caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
		HandleMessage(store, m.Subject, m.Data)

	})
	if err != nil {
		log.Printf("")
	}

	// wait for cancel
	for {
		log.Printf("heartbeat scanner enter loop (wait for cancel)")
		select {
		case <-ctx.Done(): // Done  this context is canceled -> exit
			log.Println("heartbeat scanner Exiting")
			return err
		}
	}
}

// handle uptime message with subject event.*.*.uptime  and body {"count":int}
func HandleMessage(heartbeater Store, subject string, message []byte) {

	// split subject to extract service name  ( Heartbeat.lbservices or Heartbeat.sniffer.1
	parts := strings.Split(subject, ".")
	if len(parts) <= 1 {
		// should not be there
		log.Printf("Heartbeat scanner : bad subject:%s\n", subject)
		return
	}
	// eg sniffer.1
	service := strings.Join(parts[1:], ".")

	// set timestamp for this service
	log.Printf("Heartbeat scanner set heartbeat for service : %s\n", service)
	_ = heartbeater.Set(service)

	// create prometheus metric
	m := Message{}
	err := json.Unmarshal(message, &m)
	if err == nil {
		RecordUptime(float64(m.Uptime), service)
	}
}
