package _nping

import (
	"github.com/nats-io/go-nats"
	"time"
)

/*

	send a ping to nats service


	usage:

		c,_ = NewClient( "nats://127.0.0.1:4222" )
		ok,_ = c.Ping(service,1)

		defer c.Close()

*/

func Ping(nc *nats.Conn, service string, timeout int) (bool, error) {

	topic := pingSubject(service)
	if timeout == 0 {
		timeout = 1
	}
	wait := time.Duration(timeout) * time.Second

	_, err := nc.Request(topic, []byte(pingBody), wait)
	if err != nil {
		// if err is timeout return false and no error ( "nats: timeout" )
		if err.Error() == natsTimeoutError {
			return false, nil
		}
		return false, err
	}
	// no error
	return true, nil
}
