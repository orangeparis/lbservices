package dora

import (
	//netscan "bitbucket.org/orangeparis/netscan/pkg"

	netscan "bitbucket.org/orangeparis/netscan/model"

	"context"
	"fmt"
	"log"
	"sync"
	"time"

	nats "github.com/nats-io/nats.go"
)

/*

	create the dora event ( depends on sniffer events)
		see bitbucket.org/orangeparis/sniffer/

	emits a dora event
	subject:
		event.livebox.fti.<livebox_fti>.dora

	message :
		map[string]interface{}  like sniffer dhcp ack


	task:

		the task subscribe to sniffer events of type
		sniffer.<snifferID>.<srcMac>.dhcp.<dhcp_tag>

		subscribes to sniffer.*.*.dhcp.*

		if dhcp_tag == request and srcMac correspond to a livebox
			if fti field available store the message under the srcMax index

		if dhcp_tag == reply and srcMac correspond to a livebox
			if dhcp_tag == 'ack'
				// we got a dora event
				search stored request with srcMac


*/

// DefaultDhcpRequestAckTimeout timeout between dhcp Request and dhcp Ack
var DefaultDhcpRequestAckTimeout time.Duration = 60 * time.Second

// sniffer subscription  sniffer.<snifferID>.<MacAddr>.dhcp.<dhcpTag>
var snifferSubscription = "sniffer.*.*.dhcp.>"

// EventMaker : make a dora event from sniffer dhcp messages
type EventMaker struct {
	*nats.Conn
	natsURL string // the nats url

	// sequence handler for sniffer dhcp message
	DhcpSequence *netscan.Sequence

	timeout time.Duration // time max  between a dhcp Request and the dhcp Ack

	sniffer chan *nats.Msg // sniffer channel

	// store for a sniffer dhcp event ( key is livebox mac address)
	store map[string]snifferEvent
}

// NewEventMaker create a dora eventmaker
func NewEventMaker(natsURL string) (m *EventMaker, err error) {

	// Set the error handler when creating a connection.
	// try to open nats connection
	nc, err := nats.Connect(natsURL, nats.ErrorHandler(natsErrHandler))
	if err != nil {
		log.Printf("Dora Eventmaker: cannot connect to nats server at %s", natsURL)
		return m, err
	}
	log.Printf("Dora EventMaker: connected to nats server at %s\n", natsURL)
	sequence := &netscan.Sequence{}
	timeout := DefaultDhcpRequestAckTimeout
	sniffer := make(chan *nats.Msg, 500)
	store := make(map[string]snifferEvent)
	m = &EventMaker{nc, natsURL, sequence, timeout, sniffer, store}

	return m, err

}

// Run : run the task
func (m *EventMaker) Run(ctx context.Context, wg *sync.WaitGroup) {

	// tell when leaving
	defer wg.Done()

	// subscribe to sniffer.*.*.dhcp.*
	log.Printf("Dora EventMaker: subscribes to %s\n", snifferSubscription)
	snifferSub, _ := m.ChanSubscribe(snifferSubscription, m.sniffer)
	defer snifferSub.Unsubscribe()

	// read sniffer channel
	log.Printf("Dora EventMaker: enter loop\n")

	for {

		select {
		case msg := <-m.sniffer:
			// we receive a sniffer dhcp message
			log.Printf("Dora EventMaker: receive a sniffer dhcp message\n")
			m.RunOnce(msg)

		case <-ctx.Done():
			// parent ask to quit
			log.Printf("Dora EventMaker: asked to exit\n")
			log.Printf("Dora EventMaker: exit loop\n")
			return
		}

	}

}

// RunOnce : run the task
func (m *EventMaker) RunOnce(snifferMsg *nats.Msg) {

	var macAddr string
	var dhcpTag string

	// extract subject info from sniffer message of type sniffer.$name.$device.dhcp.$tag.$sequence
	subject, err := netscan.NewReceivedSubject(snifferMsg.Subject)
	if err != nil {
		// bad subject format
		log.Printf("Dora EventMaker: %s\n", err.Error())
		return
	}

	// analyse sequence
	sequenceStatus := m.DhcpSequence.Feed(subject.Sequence)
	log.Printf("Dora EventMaker: sequence status: %s\n", sequenceStatus)

	macAddr = subject.DeviceId
	dhcpTag = subject.MsgTag

	//// is this macaddr corresponds to a livebox ?
	//if livebox.IsLivebox(macAddr) == false {
	//	// not a livebox ignore it
	//	log.Printf("Dora EventMaker: not a livebox mac [%s] : ignore it\n", macAddr)
	//	return
	//}

	log.Printf("Dora EventMaker: receive sniffer msg for livebox mac %s of type %s\n", macAddr, dhcpTag)
	switch dhcpTag {
	case "Discover":
		// ignore it
		return
	case "Offer":
		// ignore it
	case "Request":
		// this is a sniffer dhcp request ( either a Discover or Request)
		m.HandleDhcpRequest(macAddr, snifferMsg)
	case "Ack":
		// this is a sniffer dhcp reply ( a ack )
		m.HandleDhcpAck(macAddr, snifferMsg)
	default:
		// unknown dhcp tag ( should be request or reply )
		log.Printf("Dora EventMaker: received unknown sniffer dhcptag: %s\n", dhcpTag)
		return
	}

}

// HandleDhcpRequest  ( a Discover or Request)
func (m *EventMaker) HandleDhcpRequest(macAddr string, msg *nats.Msg) {
	log.Printf("Dora EventMaker: handle a sniffer dhcp request for %s\n", macAddr)

	// store the request event ( to retrieve fti field when Ack will come)
	//ev := snifferEvent{Data: msg.Data, Timestamp: time.Now()}
	ev, err := NewSnifferEvent(msg.Data)
	if err == nil {
		// see if traffic is on vlan
		_, ok := ev.Request["lanId"]
		if ok {
			// traffic is on vlan : we got our request
			//fmt.Printf("vlan:%d",vlan)
			m.store[macAddr] = *ev
		} else {
			// not on a vlan : this the request from nokia to juniper: ignore it
		}
	}

}

// HandleDhcpAck  ( a Ack )
func (m *EventMaker) HandleDhcpAck(macAddr string, msg *nats.Msg) {
	log.Printf("Dora EventMaker: handle sniffer dhcp reply for %s\n", macAddr)

	// see if we have a valid dhcp request stored to get the fti field
	history, ok := m.store[macAddr]
	if ok == false {
		// no previous dhcp request stored for this livebox : ignore it
		log.Printf("Dora Event: no previous dhcp request stored for for livebox %s\n", macAddr)
		return
	}
	// we have a dhcp request stored see if timestamp is valid
	diff := time.Now().Sub(history.Timestamp).Seconds()
	//fmt.Printf("diff:%f\n", diff)
	//fmt.Printf("timeout:%f\n", m.timeout.Seconds())

	if diff > m.timeout.Seconds() {
		// req stored is too old ignore it
		log.Printf("Dora EventMaker: sniffer request stored too old: %f\n", diff)
		return
	}
	// we got a valid dhcp request for this reply
	requestData := history.Request

	// store the Ack in history
	err := history.AddAck(msg.Data)
	if err != nil {
		return
	}
	// check if the ack is within a vlan ( otherwise we got the inter-equipment Ack )
	_, ok = history.Ack["lanId"]
	if !ok {
		// traffic is not on vlan : discard it
		return
	}

	// extract fti field from request
	fti, ok := requestData["fti"]
	if ok == false {
		// no fti
		subject := fmt.Sprintf("event.livebox.mac.%s.dora", macAddr)
		err := m.Publish(subject, msg.Data)
		if err != nil {
			log.Printf("Dora EventMaker: fail to publish dora event : %s: %s\n", subject, err.Error())
			return
		}
		log.Printf("Dora EventMaker: publish a dora event : %s\n", subject)
		return
	}
	// we have extracted fti from the previous request so we can publish dora event
	subject := fmt.Sprintf("event.livebox.fti.%s.dora", fti)
	history.UpdateAckWithRequest("fti", "AutoFallBack", "ForcedPPP", "IPTVRunningInterface", "VendorClassIdentifier")

	data, err := history.GetAck()
	if err != nil {
		log.Printf("Dora EventMaker: fail to convert Ack to json :%s\n", err.Error())
		return
	}

	err = m.Publish(subject, data)
	if err != nil {
		log.Printf("Dora EventMaker: fail to publish dora event : %s: %s\n", subject, err.Error())
		return
	}
	log.Printf("Dora EventMaker: publish a dora event : %s:\n%s\n", subject, data)

}

// utils

func natsErrHandler(_ *nats.Conn, sub *nats.Subscription, natsErr error) {
	fmt.Printf("error: %v\n", natsErr)
	if natsErr == nats.ErrSlowConsumer {
		pendingMsgs, _, err := sub.Pending()
		if err != nil {
			fmt.Printf("couldn't get pending messages: %v\n", err)
			return
		}
		fmt.Printf("Falling behind with %d pending messages on subject %q.\n",
			pendingMsgs, sub.Subject)
		// Log error, notify operations...
	}
	// check for other errors
}
