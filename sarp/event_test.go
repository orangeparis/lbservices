package sarp_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"bitbucket.org/orangeparis/lbservices/config"
	dora "bitbucket.org/orangeparis/lbservices/sarp"
)

var sampleSnifferDhcpRequest = `{"fti":"fti/exampl"}`
var sampleSnifferDhcpReply = `{"DstMac":"c0:d0:44:d8:5c:90"}`

var messages = []string{

	// a request
	"sniffer.pcap.02:00:23:c5:1c:00.dhcp.Request.49",
	`{"ClientHWAddr":"02:00:23:c5:1c:00","DHCPAuthentication":"\u0000\u0000\u0000\u001a\t\u0000\u0000\u0005X\u0001\u0003A\u0001\rfti/a3thr4a\u003c\u00125X81GqFC1m,M3~#0\u0003\u00134\ufffd J!)H","DHCPCircuitID":"DSLAN286!11!atm!08/15:8.32","DHCPMsgType":"Request","DHCPRemoteID":"00000018945006","DstIP":"255.255.255.255","DstMAC":"ff:ff:ff:ff:ff:ff","DstPort":67,"SrcIP":"0.0.0.0","SrcMAC":"02:00:23:c5:1c:00","SrcPort":68,"VendorClassIdentifier":"sagem","_verdict":"PASS","dhcpOp":"Request","fti":"fti/a3thr4a","ip4Len":478,"lanId":2900,"type":2048,"udpLen":458}`,

	// a ack

	"sniffer.pcap.02:00:23:c5:1c:00.dhcp.Ack.51",
	`{"AutoFallBack":false,"ClientHWAddr":"02:00:23:c5:1c:00","DHCPAuthentication":"\u0000\u0000\u0000dhcpli","DHCPCircuitID":"DSLAN286!11!atm!08/15:8.32","DHCPMsgType":"Ack","DHCPRemoteID":"00000018945006","DstIP":"193.253.20.226","DstMAC":"a4:7b:2c:e4:d7:77","DstPort":67,"ForcedPPP":false,"IPTVRunningInterface":"0x1","SrcIP":"172.20.123.118","SrcMAC":"3c:61:04:37:0c:fe","SrcPort":67,"_verdict":"PASS","dhcpOp":"Reply","ip4Len":508,"udpLen":488}`,
}

func TestSarpEvent(t *testing.T) {

	// sets nats utl
	url := config.SetNatsURL(config.DefaultNatsURL)
	//url := "nats://localhost:4222"

	// create dora event maker
	em, err := dora.NewEventMaker(url)
	if err != nil {
		t.Fail()
		return
	}
	em.Publish("test.dora", []byte("Dora testing"))

	// send a fake sniffer message
	go func() {
		time.Sleep(2 * time.Second)
		requestSubject := messages[0]
		requestMessage := messages[1]

		//em.Publish("sniffer.1.c0:d0:44:d8:5c:90.dhcp.Request", []byte(sampleSnifferDhcpRequest))
		em.Publish(requestSubject, []byte(requestMessage))
		time.Sleep(2 * time.Second)

		ackSubject := messages[2]
		ackMessage := messages[3]

		//em.Publish("sniffer.1.c0:d0:44:d8:5c:90.dhcp.Ack", []byte(sampleSnifferDhcpReply))
		em.Publish(ackSubject, []byte(ackMessage))
	}()

	timeout, _ := time.ParseDuration("10s")
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	time.Sleep(1 * time.Second)

	var wg sync.WaitGroup
	wg.Add(1)

	// launch task
	go em.Run(ctx, &wg)

	wg.Wait()

	cancel()

	em.Close()

}
