package main

import (
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
	"time"

	"bitbucket.org/orangeparis/ines/heartbeat"
	//"fmt"
	"net/http"
)

/*
	a demo httpserver implementing a false heartbeat scanner and metrics


	try these url

	/   -> home
	/heartbeat/            -> 400 BAD request
	/heartbeat/sniffer.1   -> 200 OK    heartbeat of service sniffer.1
	/heartbeat/lbservices  -> 404 not found

	/metrics   -> metrics prometheus , see "uptime" metric



*/

var natsServer = "nats://127.0.0.1:4222"

func main() {

	http.HandleFunc("/", HelloServer)

	// start a heartbeat scanner
	// add /heartbeat/ handler
	// add /metrics handler
	heartbeat.Setup(natsServer, 2)

	// start fake heartbeat generator
	go fakeInjector(natsServer)

	http.ListenAndServe(":8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
}

// inject fake heartbeat messages for test
func fakeInjector(server string) {

	nc, err := nats.Connect(server)
	if err != nil {
		log.Printf("injector cannot open nats connection: %s", err.Error())
		return
	}
	defer nc.Close()
	msg := fmt.Sprintf("injector connected with nats at : %s\n", server)
	log.Println(msg)

	subject := "Heartbeat.sniffer.1"

	for i := 0; i < 100; i++ {

		payload := fmt.Sprintf(`{"uptime":%d}`, i)
		nc.Publish(subject, []byte(payload))
		time.Sleep(1 * time.Second)
	}

}
