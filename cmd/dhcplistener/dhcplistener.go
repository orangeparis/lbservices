package main

import (
	"bitbucket.org/orangeparis/sniffer"
	"flag"
	"log"
	"os"
	"os/signal"

	"github.com/nats-io/go-nats"
)

/*

	a listener for sniffer dhcp publisher

	listen.$name.$device.dhcp.$tag.$seq


*/

//
// Sequence
//

//type Sequence struct {
//	Next int
//}
//
//// Feed : proceed a sequence (status:  OK/RESET/AHEAD/LATE/NONE
//func (s *Sequence) Feed(seq int) (status string) {
//
//	status = ""
//	if seq == s.Next {
//		// The nominal case
//		status = "OK"
//		// we are in sequence
//	} else {
//		// we are out sequence
//		if seq == 0 {
//			// publisher explicitly start a new sequence
//			status = "RESET"
//			// sequence has been reset by publisher ( cannot now if we missed messages
//		} else {
//			if seq < 0 {
//				// we dont have a sequence
//				status = "NONE"
//			} else {
//				// the out sequence cases , not a reset
//				if seq > s.Next {
//					status = "AHEAD"
//					// the sequence received is ahead of expected, we may ave missed message(s)
//				} else {
//					status = "LATE"
//					// the sequence received is before of expected, we get a later message ( maybe a missed one)
//				}
//			}
//		}
//	}
//	// in all case except -1 (no sequence), next is received sequence +1
//	if seq >= 0 {
//		s.Next = seq + 1
//	}
//	return status
//}
//
////
//// the sniffer dhcp subject
////
//
//var SubscribeComponents string = "EmitterType.EmitterName.DeviceId.MsgType.MsgTag.Sequence"
//
//var SubscribePattern string = "sniffer.*.*.dhcp.>"
//
//// Subject : the sniffer subject
//type Subject struct {
//	EmitterType string // sniffer
//	EmitterName string // name of emitter eg : s1
//
//	DeviceId string // usualy a mac address
//
//	MsgType  string // eg dhcp
//	MsgTag   string // a tag , can be sub type eg request/reply
//	Sequence int
//}
//
//// NewReceivedSubject : Create a subject object from a received message subject
//func NewReceivedSubject(subject string) (s *Subject, err error) {
//
//	s = &Subject{}
//	err = s.Parse(subject)
//	return s, err
//}
//
//func (s *Subject) String() string {
//	if s.Sequence >= 0 {
//		// got a valid sequence
//		return fmt.Sprintf("%s.%s.%s.%s.%s.%d",
//			s.EmitterType, s.EmitterName, s.DeviceId, s.MsgType, s.MsgTag, s.Sequence)
//	}
//	// no sequence:
//	return fmt.Sprintf("%s.%s.%s.%s.%s",
//		s.EmitterType, s.EmitterName, s.DeviceId, s.MsgType, s.MsgTag)
//
//}
//
//func (s *Subject) Parse(subject string) (err error) {
//	parts := strings.Split(subject, ".")
//	if len(parts) < 5 {
//		// bad sniffer subject format
//		err = errors.New("INVALID_FORMAT")
//		return err
//	}
//	s.EmitterType = parts[0]
//	s.EmitterName = parts[1]
//	s.DeviceId = parts[2]
//	s.MsgType = parts[3]
//	s.MsgTag = parts[4]
//	s.Sequence = -1 // means we dont yet have a sequence indicator
//
//	if len(parts) > 5 {
//		// set sequence indicator
//		seq, err := strconv.Atoi(parts[5])
//		if err != nil {
//			err = errors.New("INVALID_FORMAT:SEQUENCE")
//			return err
//		}
//		s.Sequence = seq
//	}
//	return err
//}

//
//	Listener
//

var (
	natsURL = flag.String("nats", "nats://demo.nats.io:4222", "nats server (eg nats://demo.nats.io:4222)")
	subject = flag.String("subject", "sniffer.*.*.dhcp.>", "nats subscription eg sniffer.>")
)

func run() {

	flag.Parse()
	topic := *subject

	// After setting everything up!
	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	// Run cleanup when signal is received
	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan struct{})
	signal.Notify(signalChan, os.Interrupt)

	nc, err := nats.Connect(*natsURL)
	if err != nil {
		log.Printf("nats Listener failed :%s\n", err.Error())
		return
	}
	defer nc.Close()
	log.Printf("listener : connected to nats server : %s\n", *natsURL)

	// create a sequence
	sequence := &sniffer.Sequence{}

	// subscribe to all inputs
	log.Printf("listener : set subscribe topic to: %s\n", topic)
	counter := 0
	nc.Subscribe(topic, func(m *nats.Msg) {
		log.Printf("Dhcp Listener: caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")

		// analyse subject
		receivedSubject, err := sniffer.NewReceivedSubject(m.Subject)
		if err != nil {
			log.Printf("Dhcp Listener: %s", err.Error())
			return
		}
		// Handle sequence indicator
		sequenceStatus := sequence.Feed(receivedSubject.Sequence)
		log.Printf("Dhcp Listener: sequence status is %s\n", sequenceStatus)
	})

	log.Printf("listener : starting")

	// wait for CTRL+C
	<-signalChan

	// leaving
	log.Printf("Listener have received: %d", counter)
	nc.Close()
	close(cleanupDone)

	// wait for cleanup
	<-cleanupDone

}

func main() {
	run()
}
