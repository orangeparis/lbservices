# SARP  dhcpv6 Sollicit/Advertise/Request/Reply


detecting SARP sequence for a livebox


## resources

lbservices.sarp


## methods

call.lbservices.sarp.wait

in :  {  fti: , timeout:    }
out:  { ack packet + livebox properties}

