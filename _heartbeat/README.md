# Heartbeat

a module to handle heartbeat on nats-server


   message are emitted on subject Heatbeat.<id> with a json payload {"uptime":int}



## Emitter

periodically emit a Heartbeat message

usage:

    e, _ := NewEmitter("sniffer.1", "nats://localhost:4222", 2)
    e.Start()


will emit nats message {"uptime":int} 
on subject Heartbeat.sniffer.1 every 2 seconds


## Subscriber

    import (
        "bitbucket.org/orangeparis/lbservices/heartbeat"
        "net/http"
        
    )
    
    var natsServer = "nats://127.0.0.1:4222"
    
    func main() {
    
        http.HandleFunc("/", HelloServer)
    
        // start a heartbeat scanner
        // add /heartbeat/ handler
        // add /metrics handler
        
        heartbeat.Setup(natsServer, 2)
    
    
        http.ListenAndServe(":8080", nil)
    }

  
