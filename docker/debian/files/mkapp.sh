set -x

cd /go/src/app

# add services:  sniffer + lbservices + listener
cp sniffer/sniffer /app/sniffer
cp lbservices/lbservices /app/lbservices
cp listener/listener /app/listener

# add core components : nats resgate
cp nats-server-v2.0.4-linux-amd64/nats-server /app/nats-server
cp resgate-v1.3.0-linux-amd64/resgate /app/resgate

# add tools
cp run.sh /app/run.sh

# add raml file to package
cp ../bitbucket.org/orangeparis/lbservices/lbservices.raml /app