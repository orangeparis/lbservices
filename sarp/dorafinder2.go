package sarp

/*

	TODO: convert to SARP doc

	find a dora2 event (where the mac address of the livebox is virtual eg does not belong to sagem,sercom..)

	dhcp protocol

	D discover			from livebox to broadcast 	( clientMacAddress , fti )
	O Offer				from equipment to broadcast ( clientMacAddress )
	R Request			from device to equipment   	( clientMacAddress, transactionId , fti )
	A Ack				from equipment to broadcast ( clientMacAddress, transactionId )

	a livebox dora2 sequence is made of 8 steps ( each have same ClientHWAddr = <macx> and transactionId  )

	1) a livebox <macx> send a DISCOVER to "broadcast"  			( vlan,   fti  )
	2) a nokia relay it in sending a DISCOVER to a juniper 			( no-vlan, fti )
	3) the juniper send  an OFFER to the nokia						( no-vlan )
	4) the nokia relay it in sending an OFFER to "broadcast"  		( vlan )
	5) the livebox intercept it and send a REQUEST to " broadcast"  ( vlan,   fti  )
	6) the nokia relay it in sending REQUEST to a juniper			( no-vlan  )
	7) the juniper send an ACK to the nokia							( no-vlan )
	8) the nokia send an ACK to "broadcast"							( vlan )

	at some point the livebox catch this ACK , the we can not trace it

	notes:
		all traffic between nokia and juniper are "direct" ( no vlan )
		all broadcast are on a vlan

	if we keep only vlan traffic we can simplify the sequence in 4 steps

	1) a livebox <macx> send a DISCOVER to "broadcast"  			( vlan,   fti  )
	4) the nokia sending an OFFER to "broadcast"  					( vlan )
	5) the livebox intercept it and send a REQUEST to "broadcast"  	( vlan,   fti  )
	8) the nokia send an ACK to "broadcast"							( vlan )

	the livebox catch it

	if we only want the REQUEST-ACK sequence to get rid of all the unresponded DISCOVER sent by other device

	5) the livebox intercept it and send a REQUEST to "broadcast"  ( vlan,   fti  )
	8) the nokia send an ACK to "broadcast"							( vlan )

*/

//type DhcpMacClient string
//
//type DoraSequence struct {
//	ClientHWAddr string
//	Started      time.Duration
//	Discover     []byte // store here the discover ( client to broadcast )
//	Offer        []byte
//	Request      []byte // store here the Request ( livebox to broadcast )
//	Ack          []byte // store here the Ack     ( equipment to broadcast )
//}
//
//func NewDoraSequence(clientHWAddr string) *DoraSequence {
//	return &DoraSequence{ClientHWAddr: clientHWAddr}
//}
//
//type DoraSequenceStore map[DhcpMacClient]DoraSequence
