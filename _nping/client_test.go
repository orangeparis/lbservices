package _nping

import (
	"testing"
	"time"
)

func Test_PingService(t *testing.T) {

	server := "nats://127.0.0.1:4222"

	c, err := NewClient(server)
	if err != nil {
		t.Errorf("failed to create client : %s", err.Error())
		return
	}

	// handle a pong server for service_1
	sub, _ := c.HandlePingService("service1")
	defer sub.Unsubscribe()

	// call ping service
	p1, _ := c.Ping("unknown_service", 1)
	if p1 != false {
		t.Error("expected a false response")
		return
	}

	// call ping service
	p2, _ := c.Ping("service1", 1)
	if p2 == false {
		t.Error("expected a true response")
		return
	}

	// try to handle a non free service
	_, err = c.HandlePingService("service1")
	if err != nil {
		// error should be  Service [%s] already in use : ABORT"
		if err.Error() == "Service [service1] already in use : ABORT" {
			// the message we waited for
		} else {
			t.Errorf("unexpected error :%s", err.Error())
			return
		}

	} else {
		// we are not supposed to handle a busy ping service
		t.Errorf("expected an error: shouldn handle a busy ping service")
		return
	}

	time.Sleep(1 * time.Second)

}
