package _nping

import (
	"github.com/nats-io/go-nats"
	"github.com/pkg/errors"
)

/*

	handle ping service


	usage:

		c,_ = NewClient( "nats://127.0.0.1:4222" )
		defer c.Close()

		sub , _ := c.HandlePingService( "service" )
		defer sub.Unsubscribe()


*/

// subscribes to get.<service>.ping
// and respond with PONG
func HandlePingService(nc *nats.Conn, service string) (sub *nats.Subscription, err error) {

	subject := pingSubject(service)

	// try to ping service , to check it is not already in use
	p, err := Ping(nc, service, 1)
	if p == true {
		err = errors.Errorf("Service [%s] already in use : ABORT", service)
		return sub, err
	}
	if err != nil {
		// an error which is not a timeout
		return sub, err
	}

	// ping service is free : handle it
	sub, err = nc.Subscribe(subject, func(m *nats.Msg) {
		_ = nc.Publish(m.Reply, []byte(pongBody))
	})

	return sub, err
}
