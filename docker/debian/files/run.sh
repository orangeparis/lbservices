set -x

echo "start nats server"
./nats-server &

sleep 2

echo "start nats http gateway ont port 8080"
./resgate --nats://127.0.0.1:4222  --port 8080 --reqtimeout 3000   &

echo "start sniffer"
./sniffer --name s1 --nats://127.0.0.1:4222 --interface eth1 &


echo "start lbservices"
./lbservices --nats://127.0.0.1:4222


#echo "start listener"
#./listener --nats://127.0.0.1:4222 --prefix ""