
# an image to build a demo : nats-server,resgate,sniffer,lbservices,listener

because sniffer use gopacket and go packet needs libcap 
so could not cross compile it from osx




# build image sniffer

    ./build.sh 

# run it interactive

    docker run -it --rm sniffer 

# build package on local $HOME/app

docker run -it --rm -v ~/app:/app sniffer --cmd "./mkapp.sh"

## to transfer from container to $HOME/app

    docker run -it --rm -v ~/app:/app sniffer
    cp sniffer /app/sniffer

then back to the host get

    $HOME/app/sniffer





# nats server

download from 

    wget https://github.com/nats-io/nats-server/releases/download/v2.0.4/nats-server-v2.0.4-linux-amd64.zip
