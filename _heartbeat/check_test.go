package _heartbeat

import (
	"github.com/nats-io/nats"
	"testing"
	"time"
)

func TestCheckHeartbeat(t *testing.T) {

	natsServer := "nats://127.0.0.1:4222"

	service := "myservice.1"

	nc, err := nats.Connect(natsServer)
	if err != nil {
		t.Errorf("no nats server: %s", err.Error())
	}

	go func() {
		time.Sleep(10 * time.Second)
		body := `"uptime":12}`
		nc.Publish("Heartbeat.myservice.1", []byte(body))

	}()

	ok, err := CheckHeartbeat(natsServer, service, 5)
	if err != nil {
		t.Errorf("failed : %s", err.Error())
	}

	if ok != true {
		t.Errorf("failed ...")
	}

}
