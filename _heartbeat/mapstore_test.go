package _heartbeat

import (
	"testing"
	"time"
)

func TestMapStore(t *testing.T) {

	service := "sniffer.1"
	delay := 1

	h := NewMapStore(delay)

	// set timestamp for the service
	h.Set(service)

	// check
	rok := h.IsAlive(service)
	if rok != true {
		t.Fail()
		return
	}

	// check an unknown service
	rko := h.IsAlive("service.unknown")
	if rko != false {
		t.Fail()
		return
	}

	// test obsolete time stamp
	time.Sleep(2 * time.Second)
	if rko != false {
		t.Fail()
		return
	}

}
