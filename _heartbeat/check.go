package _heartbeat

import (
	"context"
	"github.com/nats-io/go-nats"
	"log"
	"time"
)

/*

	a check heartbeat function

	CheckHertbeat ( nats string , service service, timeout int ) ( bool )

*/

const defaultTimeout = 10

func CheckHeartbeat(natsServer string, service string, timeout int) (bool, error) {

	var wait time.Duration

	if timeout == 0 {
		timeout = defaultTimeout
	}
	// set waiting time
	wait = time.Duration(timeout) * time.Second

	received := false

	// open a nats connection
	nc, err := nats.Connect(natsServer)
	if err != nil {
		log.Printf("Heartbeat checker: cannot open nats connection: %s", err.Error())
		return false, err
	}
	defer nc.Close()

	ctx, cancel := context.WithCancel(context.Background())

	// set topic eg Heartbeat.lbservices
	topic := RootTopic + "." + service
	// Simple Async Subscriber
	sub, err := nc.Subscribe(topic, func(m *nats.Msg) {
		log.Printf("Heartbeat checker :Received a message:[%s] %s\n", m.Subject, string(m.Data))
		received = true
		cancel()
	})
	if err != nil {
		log.Printf("Heartbeat checker : cannor subscribe to :[%s] \n", topic)
		return false, err
	}
	defer sub.Unsubscribe()

	// wait for
	select {
	case <-ctx.Done():
		// we received a message
		received = true
	case <-time.After(wait):
		log.Printf("Heartbeat checker: timeout waiting for %s\n", topic)
		received = false
	}

	return received, nil
}
