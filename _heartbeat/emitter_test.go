package _heartbeat

import (
	"context"
	"github.com/nats-io/go-nats"
	"log"
	"testing"
	"time"
)

func TestEmitter(t *testing.T) {

	natsServer := "nats://127.0.0.1:4222"

	ctx, cancel := context.WithCancel(context.Background())

	// start emitter
	e, err := NewEmitter("sniffer.1", natsServer, 1)
	if err != nil {
		t.Fail()
		return
	}

	e.Start()

	// start listener
	go Listener(ctx, natsServer, "")

	time.Sleep(10 * time.Second)
	cancel()

}

func Listener(ctx context.Context, natsUrl string, topic string) (err error) {

	if topic == "" {
		// default topic : all
		topic = ">"
	} else {
		// modify topic to catch all from root topic
		topic = topic + ".>"
	}
	nc, err := nats.Connect(natsUrl)
	if err != nil {
		log.Printf("nats Listener failed :%s\n", err.Error())
		return err
	}

	defer nc.Close()

	// subscribe to all inputs
	nc.Subscribe(topic, func(m *nats.Msg) {
		//log.Printf("Listener catch a message: %s on  subject: %s\n", string(m.Data), m.Subject)
		log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")

	})

	// wait for cancel
	for {
		log.Printf("nats Listener enter loop (wait for cancel)")
		select {
		case <-ctx.Done(): // Done  this context is canceled -> exit
			log.Println("nats listener Exiting")
			return err
		}
	}

}
