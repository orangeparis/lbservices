package dora

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/orangeparis/lbservices/config"
	"github.com/jirenius/go-res"
	nats "github.com/nats-io/nats.go"
)

/*


	livebox.dora.wait { fti , timeout }  -> { doraEvent }

*/

// WaitIn : params for call.livebox.dora.wait
type WaitIn struct {
	Fti     string `json:"fti"`     // livebox fti account
	Mac     string `json:"mac"`     // livebox mac address
	Timeout uint64 `json:"timeout"` // timeout in seconds
}

// Model : DoraModel
type Model struct {
	Message string `json:"message"`
}

// The model we will serve
var shared = &Model{Message: "Hello, Go World!"}

// LiveboxDoraService :  handle resource livebox.dora
func LiveboxDoraService(s *res.Service) {

	s.Handle("dora",
		res.Access(res.AccessGranted),

		// set the service livebox.dora.wait ( mac,timeout ) -> doraEvent
		res.Call("wait", func(r res.CallRequest) {
			var p = WaitIn{}
			r.ParseParams(&p)

			// set timeout in seconds
			timeout := time.Duration(p.Timeout) * time.Second

			// extend service timeout to match requested timeout
			serviceTimeout := time.Duration(p.Timeout+2) * time.Second

			// extend service timeout to match requested timeout
			r.Timeout(serviceTimeout)

			// wait for dora event
			msg, err := waitForDora(p.Fti, timeout)
			if err != nil {
				r.NotFound()
				return
			}
			_ = msg
			//resp := fmt.Sprintf("dora request received for livebox: %s", p.Fti)
			//response := fmt.Sprintf(`{"result":%s]`, msg.Data)
			//r.OK(string(msg.Data))
			response := make(map[string]string)
			err = json.Unmarshal(msg.Data, &response)
			if err != nil {
				rerr := &res.Error{Code: "500", Message: err.Error()}
				r.Error(rerr)
			}
			r.OK(response)
		}),

		// respond to lbservices.dora.ping
		res.Call("ping", func(r res.CallRequest) {
			r.OK("PONG")
		}),
	)

}

// waitForDora subscribe to event event.livebox.mac.<liveboxMac>.dora and wait for msg or timeout
func waitForDora(id string, timeout time.Duration) (msg *nats.Msg, err error) {

	// set a nats server subscription from global nats url
	url, err := config.GetNatsURL()
	if err != nil {
		return msg, err
	}
	// Connect to the config nats server
	nc, err := nats.Connect(url)
	if err != nil {
		return msg, err
	}
	defer nc.Close()

	subject := fmt.Sprintf("event.livebox.fti.%s.dora", id)
	ch := make(chan *nats.Msg)

	// ChanSubscribe subscribes to messages matching the subject pattern.
	println("waitForDora: subscribe to " + subject)
	sub, err := nc.ChanSubscribe(subject, ch)
	if err != nil {
		return msg, err
	}
	defer sub.Unsubscribe()

	//
	select {
	case msg := <-ch:
		// we receive a dora message
		println("waitForDora: receive a dora message on: " + subject)
		return msg, nil
	case <-time.After(timeout):
		// no message received within timeout
		err = errors.New("WaitForDora timeout: no dora event")
		return msg, err
	}
}
