package _heartbeat

import (
	"context"
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
	"testing"
	"time"
)

func TestScanner(t *testing.T) {

	//
	//natsUrl := "nats://demo.nats.io:4222"
	natsUrl := "nats://localhost:4222"

	heartbeat := NewMapStore(3)

	ctx := context.Background()

	go inject(natsUrl)
	go Scan(ctx, natsUrl, heartbeat)

	time.Sleep(10 * time.Second)

	ctx.Done()

	time.Sleep(2 * time.Second)

}

func inject(server string) {

	nc, err := nats.Connect(server)
	if err != nil {
		log.Printf("injector cannot open nats connection: %s", err.Error())
		return
	}
	defer nc.Close()
	msg := fmt.Sprintf("injector connected with nats at : %s\n", server)
	log.Println(msg)

	subject := "Heartbeat.sniffer.1"
	payload := `{"uptime":12}`

	for i := 0; i < 2; i++ {

		nc.Publish(subject, []byte(payload))
		time.Sleep(1 * time.Second)
	}

	subject = "Heartbeat.lbservices"
	nc.Publish(subject, []byte(payload))

}
