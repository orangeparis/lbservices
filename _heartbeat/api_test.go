package _heartbeat

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHealthCheckHandler(t *testing.T) {
	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("GET", "/heartbeat/sniffer.1", nil)
	if err != nil {
		t.Fatal(err)
	}

	// create a heartbeat in the store
	heartbeat := NewMapStore(10)
	// set a timestamp entry for sniffer-1
	heartbeat.Set("sniffer.1")

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.Handler(healthcheckHandler(heartbeat))
	//handler := http.HandlerFunc(HealthCheckHandler)

	//http.Handle(healthCheckRoute, healthcheckHandler(heartbeat))

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `{"alive": true}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
