# DORA  dhcp Discover/Offer/Request/Ack


detecting DORA sequence for a livebox


## resources

lbservices.dora


## methods

call.lbservices.dora.wait  

in :  {  fti: , timeout:    }
out:  { ack packet + livebox properties}

