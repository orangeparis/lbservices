package lbservices

import "errors"

/*

	set the nats url

*/

// llocal instance of nats url
var natsURL = ""

// DefaultNatsURL default value for nats server
var DefaultNatsURL = "nats://demo.nats.io:4222"

//var DefaultNatsURL = "nats://localhost:4222"

// SetNatsURL set the global nats url
func SetNatsURL(url string) string {

	if url == "" {
		url = DefaultNatsURL
	}
	// set the global nats url
	natsURL = url

	return natsURL
}

// GetNatsURL : get the global nats url setted by SetNatsUrl
func GetNatsURL() (url string, err error) {
	if natsURL == "" {
		err = errors.New("No nats url defined by SetNatsURL")
		return url, err
	}
	return natsURL, nil
}
