
export GOPATH=~/Go
export GOARCH=amd64
export CGO_ENABLED=0


echo "build for linux-amd64 in build/packages/linux_amd64"

export GOOS=linux
echo "    compile dhcplistener"
go build -o ./build/packages/${GOOS}_${GOARCH}/dhcp-listener ./cmd/dhcplistener

echo "    compile lbservices"
go build -o ./build/packages/${GOOS}_${GOARCH}/lbservices ./cmd/lbservices

echo "    compile listener"
go build -o ./build/packages/${GOOS}_${GOARCH}/listener ./cmd/listener

echo "    copy raml"
cp ./api/lbservices.raml ./build/packages/${GOOS}_${GOARCH}


echo "build for darwin-amd64 in build/packages/darwin_amd64"

export GOOS=darwin
echo "    compile dhcplistener"
go build -o ./build/packages/${GOOS}_${GOARCH}/dhcp-listener ./cmd/dhcplistener

echo "    compile lbservices"
go build -o ./build/packages/${GOOS}_${GOARCH}/lbservices ./cmd/lbservices

echo "    compile listener"
go build -o ./build/packages/${GOOS}_${GOARCH}/listener ./cmd/listener

echo "    copy raml"
cp ./lbservices.raml ./build/packages/${GOOS}_${GOARCH}


