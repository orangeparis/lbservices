package _heartbeat

import (
	"github.com/prometheus/client_golang/prometheus"
)

// InitMetrics : declare metrics
func InitMetrics() {

	prometheus.MustRegister(Uptime)

}

// Uptime : uptime in seconds of the source
var Uptime = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "uptime",
		Help: "count of seconds since started",
	},
	[]string{"service"}, // eg  sniffer.1 or lbservices
)

//
// metrics recorders ( helpers to record metrics)
//

// RecordUptime : number of seconds since started
func RecordUptime(count float64, service string) {
	Uptime.With(prometheus.Labels{"service": service}).Set(count)
}
