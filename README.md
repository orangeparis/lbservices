# LBSERVICES #

Livebox services : a set of nats services around livebox testing based on resgate protocol : https://resgate.io 
	



## Livebox-DORA

	detect dora sequence on a livebox ( dhcp Discover/Offer/Request/Ack )
	
	depends on sniffer events ( repository sniffer )
	
	* subscribes to sniffer.<snifferID>.<liveboxMac>.dhcp.>
	* emits dora event  : event.livebox.mac.<liveboxMac>.dora  
	
	create nats services

	* call.livebox.dora.wait : wait for a dora event on a livebox 
	in : { mac string , timeout: int }
	out: json dict of a sniffer dhcp ack reply
	
