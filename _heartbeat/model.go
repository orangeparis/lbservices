package _heartbeat

var RootTopic = "Heartbeat"
var DefaultPeriod int = 5

type Store interface {
	Set(name string) error    // record a timestamp for this name
	IsAlive(name string) bool // check a timestamp for this name
}

type Config struct {
	Root   string // root topic : Heartbeat
	Period int    // emitter period in seconds
}

type Message struct {
	Uptime int
}
