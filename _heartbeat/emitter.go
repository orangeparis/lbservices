package _heartbeat

import (
	"fmt"
	"github.com/nats-io/go-nats"
	"log"
	"strconv"
	"time"
)

/*

	emits periodicaly heartbeat message on nats

	subject:   Heartbeat.<name>
	payload:   {"uptime":int}

*/

type Emitter struct {
	*nats.Conn
	Topic  string        // root topic to identify Heartbeat event ( Heartbeat )
	Server string        // server or list of nats servers
	Name   string        // name of the device : eg sniffer.S1
	Period time.Duration // eg 10 * time.Second

	started time.Time //
}

func (p *Emitter) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	// publish to nats
	err = p.Conn.Publish(subject, message)
	return err
}

// Start the timer
func (p *Emitter) Start() {

	go func() {

		for {
			now := time.Now()
			elapsed := now.Sub(p.started)
			message := fmt.Sprintf(`{"uptime":%d}`, int(elapsed.Seconds()))

			topic := p.Name
			p.Publish(topic, []byte(message))

			time.Sleep(p.Period)
		}
	}()

}

// NewEmitter : create a Heartbeat Emitter
func NewEmitter(name string, server string, period int) (*Emitter, error) {

	if period == 0 {
		period = DefaultPeriod // send every 5 seconds
	}
	x := strconv.Itoa(period)
	_ = x
	sPeriod, err := time.ParseDuration(strconv.Itoa(period) + "s")
	if err != nil {
		sPeriod, _ = time.ParseDuration("5s")
	}

	p := &Emitter{Name: name, Topic: RootTopic, Server: server, Period: sPeriod, started: time.Now()}
	conn, err := nats.Connect(p.Server)
	if err != nil {
		log.Printf("Heartbeat emitter: cannot open nats connection: %s", err.Error())
		return p, err
	}
	p.Conn = conn
	msg := fmt.Sprintf("Heartbeat emitter connected with : %s\n", p.Server)
	log.Println(msg)

	return p, err
}
