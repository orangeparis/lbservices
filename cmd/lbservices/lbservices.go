package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"sync"

	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/lbservices/config"
	"bitbucket.org/orangeparis/lbservices/dora"

	"github.com/jirenius/go-res"
)

/*
	runs a dora EventMaker  ( produces dora event from sniffer dhcp messages )

	runs nats services

		call.livebox.dora.wait
			in : { fti,timeout }
			out: json dict

*/

var natsURL string

func main() {

	var nats string
	flag.StringVar(&nats, "nats", "nats://127.0.0.1:4222", "nats server url")

	flag.Parse()

	// set the global nats url
	url := config.SetNatsURL(nats)
	log.Printf("lbservices: starts with nats server at : %s\n", url)

	// context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var wg sync.WaitGroup

	// create the dora EventMaker
	em, err := dora.NewEventMaker(url)
	if err != nil {
		m := fmt.Sprintf("lbservices: EventMaker cannot start :%s", err.Error())
		panic(m)
	}
	defer em.Close()
	em.Publish("lbservices.dora.EventMaker", []byte("Dora EventMaker started"))

	// starts the dora event maker
	wg.Add(1)
	go em.Run(ctx, &wg)

	// create the lbservices
	s := res.NewService("lbservices")

	// add dora resources
	dora.LiveboxDoraService(s)

	// Start the service
	go s.ListenAndServe(url)

	// start the heartbeat emiter
	e, _ := heartbeat.NewEmitter("lbservices", url, 5)
	e.Start()

	// wait here
	wg.Wait()

	log.Printf("lbservices: exit")
}
