package dora_test

import (
	"encoding/json"
	"log"
	"sync"
	"testing"
	"time"

	"bitbucket.org/orangeparis/lbservices/config"
	"bitbucket.org/orangeparis/lbservices/dora"
	"github.com/jirenius/go-res"
	nats "github.com/nats-io/nats.go"

	//"github.com/nats-io/go-nats"

	// a resgate client : a nats client with timeout handling
	"github.com/resgateio/resgate/logger"
	client "github.com/resgateio/resgate/nats"
)

var natsServer = "nats://demo.nats.io:4222"

type resResponse struct {
	Result map[string]string `json:"result"`
	Error  map[string]string `json:"error"`
}

func TestDoraServices(t *testing.T) {

	// set global nats url
	ns := config.SetNatsURL(natsServer)

	// Create a new RES Service
	s := res.NewService("lbservices")

	// add dora resources
	dora.LiveboxDoraService(s)

	// Start the service
	go s.ListenAndServe(ns)
	defer s.Shutdown()
	time.Sleep(1 * time.Second)

	// create a resgate nats client
	rcli := client.Client{
		RequestTimeout: 3 * time.Second,
		URL:            ns,
		Logger:         logger.NewStdLogger(true, true),
	}
	err := rcli.Connect()
	if err != nil {
		log.Fatal(err)
	}

	var wg sync.WaitGroup

	// create a nats publisher and send a defered dora event
	nc, err := nats.Connect(ns)
	if err != nil {
		//log.Printf("cannot connect to nats server at %s", ns)
		t.Errorf("cannot connect to nats server at %s", ns)
		return
	}
	wg.Add(1)
	go func() {
		// send a fake dora message in 30 seconds
		time.Sleep(5 * time.Second)
		event := map[string]string{"srcMac": "00:00:...", "dstMac": "00:00:..."}
		m, _ := json.Marshal(event)

		//m := "Dora ok"
		println("test publisher publish to event.livebox.fti.fti/exampl.dora")
		_ = nc.Publish("event.livebox.fti.fti/exampl.dora", []byte(m))
		wg.Done()
	}()

	// send a request
	wg.Add(1)
	payload := `{ "params":{"fti":"fti/exampl","timeout":10}}`
	rcli.SendRequest("call.lbservices.dora.wait", []byte(payload),
		func(subj string, payload []byte, err error) {
			response := &resResponse{}
			err = json.Unmarshal(payload, &response)
			if err != nil {
				t.Errorf("received invalid json response: %s", err.Error())
				return
			}
			if response.Result["srcMac"] != "00:00:..." {
				t.Errorf("received a bad dora event response")
			}
			wg.Done()
			println("received response: ", string(payload))
		})

	wg.Wait()

	println("Done")
}
