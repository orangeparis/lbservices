package _heartbeat

import (
	"context"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io"
	"net/http"
	"strings"
)

/*
	an http api for Heartbeat scanner

	starts a heartbeat scanner Heartbeat.>

	add a route /heartbeat/ to http server  pointing to heartbeat handler


	POST /heartbeat/sniffer.1 will return 200 OK if service is alive , 404 or 408 otherwise


	usage:






*/

var healthCheckRoute = "/heartbeat/"

func Route(heartbeat Store) {

	// register route for /health-check/*
	http.Handle(healthCheckRoute, healthcheckHandler(heartbeat))

	// register route for /metrics
	http.Handle("/metrics", promhttp.Handler())

}

// a closure with checker
func healthcheckHandler(checker Store) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// parse the path /heartbeat/sniffer.1
		var serviceName = ""
		parts := strings.Split(r.URL.String(), "/")
		if len(parts) >= 2 {
			serviceName = parts[2]
		}
		if serviceName == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// check service heartbeat
		alive := checker.IsAlive(serviceName)
		if alive != true {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		// A very simple health check.
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		// In the future we could report back on the status of our DB, or our cache
		// (e.g. Redis) by performing a simple PING, and include them in the response.
		io.WriteString(w, `{"alive": true}`)

	})
}

// setup metrics and heartbeat handler with a MapStore
func Setup(natsServer string, delay int) {

	// init prometheus metrics
	InitMetrics()

	// create a heartbeat store
	heartbeat := NewMapStore(delay)

	// create route for /heartbeat
	Route(heartbeat)

	// create and launch the scanner process  subscribes to Heartbeat.>
	ctx := context.Background()
	go Scan(ctx, natsServer, heartbeat)

}
