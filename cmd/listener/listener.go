package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"

	"github.com/nats-io/go-nats"
)

var (
	prefix  = flag.String("prefix", "", "prefix of the subscription eg sniffer will subscribe to sniffer.>")
	natsURL = flag.String("nats", "nats://127.0.0.1:4222", "nats server (eg nats://demo.nats.io:4222)")
)

func getSubject(prefix string) (topic string) {

	// return the full topic for subscription eg sniffer.>
	if prefix == "" {
		// no prefix => all
		return ">"
	}

	if strings.HasSuffix(prefix, ".>") {
		// topic is already full
		topic = fmt.Sprintf("%s", prefix)
	} else {
		if strings.HasSuffix(prefix, ".") {
			// add > to the topic
			topic = fmt.Sprintf("%s>", prefix)
		} else {
			// raw topic : add .>
			topic = fmt.Sprintf("%s.>", prefix)
		}
	}

	return topic
}

func run() {

	flag.Parse()

	// After setting everything up!
	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	// Run cleanup when signal is received
	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan struct{})
	signal.Notify(signalChan, os.Interrupt)

	// get a publisher corresponding to the flags ( screen or nats )
	topic := getSubject(*prefix)

	nc, err := nats.Connect(*natsURL)
	if err != nil {
		log.Printf("nats Listener failed :%s\n", err.Error())
		return
	}
	defer nc.Close()
	log.Printf("listener : connected to nats server : %s\n", *natsURL)

	// subscribe to all inputs
	log.Printf("listener : set subscribe topic to: %s\n", topic)
	counter := 0
	nc.Subscribe(topic, func(m *nats.Msg) {
		//log.Printf("Listener catch a message: %s on  subject: %s\n", string(m.Data), m.Subject)
		log.Printf("Listener : caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
		counter += 1
	})

	log.Printf("listener : starting")

	// wait for CTRL+C
	<-signalChan

	// leaving
	log.Printf("Listener have received: %d", counter)
	nc.Close()
	close(cleanupDone)

	// wait for cleanup
	<-cleanupDone

}

func main() {
	run()
}
